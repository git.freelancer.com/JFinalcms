package com.cms.controller.admin;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.cms.Feedback;
import com.cms.entity.Form;
import com.cms.entity.FormField;
import com.cms.routes.RouteMapping;
import com.cms.util.DbUtils;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;


/**
 * Controller - 表单数据
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/form_data")
public class FormDataController extends BaseController {
	
	/**
	 * 列表
	 */
	public void index(){
		setListQuery();
	    setAttr("forms", new Form().dao().findAll());
	    render(getView("form_data/index"));
	}

	/**
	 * 添加
	 */
	public void add(){
		Integer formId = getParaToInt("formId");
		Form form = new Form().dao().findById(formId);
		List<FormField> formFields = new FormField().dao().findList(formId);
		setAttr("formFields",formFields);
		setAttr("form", form);
		setAttr("formId", formId);
		render(getView("form_data/add"));
	}
	
	/**
	 * 保存
	 */
	public void save(){
		Integer formId = getParaToInt("formId");
		Form form = new Form().dao().findById(formId);
		List<FormField> formFields = new FormField().dao().findList(formId);
		Record formData = new Record();
		for(FormField formField : formFields){
			String value = getPara(formField.getName());
			formData.set(formField.getName(), value);
		}
		formData.set("createDate", new Date());
		formData.set("updateDate", new Date());
		Db.save(form.getTableName(), formData);
		renderJson(Feedback.success(new HashMap<>()));
	}
	
	/**
	 * 编辑
	 */
	public void edit() {
		Integer id = getParaToInt("id");
		Integer formId = getParaToInt("formId");
		Form form = new Form().dao().findById(formId);
		List<FormField> formFields = new FormField().dao().findList(formId);
		Record formData = Db.findById(form.getTableName(), id);
		setAttr("formFields",formFields);
		setAttr("form", form);
		setAttr("formData", formData);
		setAttr("formId", formId);
		render(getView("form_data/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		Integer id = getParaToInt("id");
		Integer formId = getParaToInt("formId");
		Form form = new Form().dao().findById(formId);
		List<FormField> formFields = new FormField().dao().findList(formId);
		Record formData = new Record();
		for(FormField formField : formFields){
			String value = getPara(formField.getName());
			formData.set(formField.getName(), value);
		}
		formData.set("id", id);
		formData.set("updateDate", new Date());
		Db.update(form.getTableName(), formData);
		renderJson(Feedback.success(new HashMap<>()));
	}
	
	/**
	 * 数据
	 */
	public void data() {
		setListQuery();
		Integer formId = getParaToInt("formId");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		Form form = new Form().dao().findById(formId);
		List<FormField> formFields = new FormField().dao().findList(formId);
		setAttr("formFields", formFields);
		String orderBySql = DbUtils.getOrderBySql("createDate desc");
		Page<Record> page = Db.paginate(pageNumber, PAGE_SIZE, "select *", "from "+form.getTableName()+" where 1=1 "+orderBySql);
		setAttr("page", page);
		setAttr("formId", formId);
		render(getView("form_data/data"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Integer formId = getParaToInt("formId");
		Form form = new Form().dao().findById(formId);
		Integer ids[] = getParaValuesToInt("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			Db.update("delete from "+form.getTableName()+" where id in("+StringUtils.join(ids, ",")+")");
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}